FROM debian:latest

RUN apt update && apt upgrade
RUN apt install rsync git wget openssh-client -y
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.111.3/hugo_0.111.3_linux-amd64.deb
RUN dpkg -i ./hugo_0.111.3_linux-amd64.deb
RUN rm hugo_0.111.3_linux-amd64.deb
RUN git clone https://github.com/colorchestra/smol /themes/smol
RUN git clone https://github.com/colorchestra/feast /themes/feast
RUN git clone https://github.com/colorchestra/hugo-infopage /themes/infopage
RUN mkdir .ssh
