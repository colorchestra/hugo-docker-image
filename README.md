Aktuell nicht automatisiert. 
- [ ] rausfinden, wie man das in Gitlab CI baut
- [ ] Schedule einrichten

Bis dahin lokal: 
```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/colorchestra/hugo-docker-image .
docker push registry.gitlab.com/colorchestra/hugo-docker-image
```